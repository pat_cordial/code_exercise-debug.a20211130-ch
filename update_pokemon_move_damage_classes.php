<?php
require_once __DIR__.'/vendor/autoload.php';
(new Dotenv\Dotenv(__DIR__))->load();

$pdo = new PDO('mysql:host=mysqlDB;dbname='.getenv('DB_DATABASE'), getenv('DB_USERNAME'), getenv('DB_PASSWORD') );

//get pokemon api config
$sth = $pdo->prepare('SELECT configvalue FROM appconfig WHERE configkey = "api_pokemon"');
$sth->execute();
$pokemon_api_paths=[];
while($val =  $sth->fetchColumn() ) //multiple values will get load balanced
{
    $pokemon_api_paths[] = $val;
}

//get pokemon type api config
$sth = $pdo->prepare('SELECT configvalue FROM appconfig WHERE configkey = "api_pokemon_type"');
$sth->execute();
while($val =  $sth->fetchColumn() ) //multiple values will get load balanced
{
    $pokemon_type_api_paths[] = $val;
}



//get all pokemon names
$sth = $pdo->prepare('SELECT id, name FROM poke');
$sth->execute();

while($poke = $sth->fetch(PDO::FETCH_ASSOC) )
{
    if( isset($start) ){echo "\tTime spent: ". round(time() - $start). 'secs';}
    $start=time();
    echo "\n". $poke['name'];
    $loadbalanced_pokemon_api = $pokemon_api_paths[mt_rand(0, count($pokemon_api_paths) - 1)];        //randomly select between configured api paths as a load-balancing mechanism
    $loadbalanced_type_api = $pokemon_type_api_paths[mt_rand(0, count($pokemon_type_api_paths) - 1)]; //randomly select between configured api paths as a load-balancing mechanism
    $pokemon = get_pokemon_info($loadbalanced_pokemon_api, $poke['name']);
    if( !$pokemon ){
        sleep(1);//rate limit so we don't break the poke api
        #fwrite(STDERR," \033[0;33m[Notice: Unable to find info on pokemon]\033[0m");
        continue;
    }
    $damage_classes = [];
    foreach($pokemon['types'] as $type){
        $move_damage_class = get_move_damage_classes_by_type($loadbalanced_type_api, $type['type']['name']);
        if( $move_damage_class ) {
            $damage_classes[$type['type']['name']] = $move_damage_class;
        }
        else {
            fwrite(STDERR," \033[0;31m[Error: Failed to find move damage class for {$type['type']['name']}]\033[0m");
        }
    }
    if( $damage_classes ){
        //we got the damage classes, update the pokemon
        $damage_classes_string = implode(',',$damage_classes);
        $updatesth = $pdo->prepare('UPDATE poke SET move_damage_classes = ?, updated_at = NOW() WHERE id = ?');
        $updatesth->bindValue(1, $damage_classes_string, PDO::PARAM_STR);
        $updatesth->bindValue(2, $poke['id'], PDO::PARAM_INT);
        $updatesth->execute();
    }
    sleep(1);//rate limit so we don't break the poke api

}
echo "\ncomplete @" . date('c')."\n";
exit;


/// Functions:
/**
 * @param $api_path string the URL to use with the API call.
 * @param $pokemon_name string the name of a pokemon
 * @return bool|assoc an assoc array describing the pokemon, or false on error.
 */
function get_pokemon_info($api_path, $pokemon_name)
{
    $ch = curl_init($api_path.$pokemon_name);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  //return result payload as string
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); //number of seconds to wait while trying to connect
    $pokemon_info = json_decode(curl_exec($ch), true);
    curl_close($ch);
    return empty($pokemon_info)? false: $pokemon_info;
}

/**
 * @param $api_path string the URL to use with the API call.
 * @param $type string a pokemon type.
 * @return bool|string either a move damage class string, or false on failure.
 */
function get_move_damage_classes_by_type($api_path, $type)
{
    $ch = curl_init($api_path.$type);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  //return result payload as string
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); //number of seconds to wait while trying to connect
    $type_info = json_decode(curl_exec($ch), true);
    curl_close($ch);
    return empty($type_info)? false: $type_info['move_damage_class']['name'];
}