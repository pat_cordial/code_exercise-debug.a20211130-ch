<?php

use Illuminate\Database\Seeder;

class appconfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //pokemon api api domains
        DB::table('appconfig')->insert( ['configkey' => 'api_pokemon', 'configvalue' => 'https://pokeapi.co/api/v2/pokemon/', 'created_at' => date('Y-m-d H:i:s')] );
        DB::table('appconfig')->insert( ['configkey' => 'api_pokemon', 'configvalue' => 'http://pokeapi.co/api/v2/pokemon/', 'created_at' => date('Y-m-d H:i:s') ] );

        DB::table('appconfig')->insert( ['configkey' => 'api_pokemon_type', 'configvalue' => 'https://pokeapi.co/api/v2/type/', 'created_at' => date('Y-m-d H:i:s') ] );
        DB::table('appconfig')->insert( ['configkey' => 'api_pokemon_type', 'configvalue' => 'http://pokeapi.co/api/v2/type/', 'created_at' => date('Y-m-d H:i:s') ] );
        DB::table('appconfig')->insert( ['configkey' => 'api_pokemon_type', 'configvalue' => 'http://localhost/tempdebug.php/' ] );
    }
}
